//
//  GameObject.hpp
//  2D_Game_Engine
//
//  Created by Damon Floyd on 12/2/18.
//  Copyright © 2018 Damon Floyd. All rights reserved.
//

#ifndef GameObject_hpp
#define GameObject_hpp

#include "Game.hpp"

class GameObject {
    
public:
    GameObject(const char* textureSheet, int x, int y);
    ~GameObject();
    
    void Update();
    void Render();
    
private:
    int xpos, ypos;
    
    SDL_Texture* texture;
    SDL_Rect src, dest;
};

#endif /* GameObject_hpp */
