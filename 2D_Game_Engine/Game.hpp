//
//  Game.hpp
//  2D_Game_Engine
//
//  Created by Damon Floyd on 12/1/18.
//  Copyright © 2018 Damon Floyd. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include "SDL2/SDL.h"
#include "/Library/Frameworks/SDL2_image.framework/Headers/SDL_image.h"
#include <iostream>

class Game {
  
public:
    Game();
    ~Game();
    
    //TODO:  move all these args to a struct
    void init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);
    
    void handleEvents();
    void update();
    void render();
    void clean();
    
    bool running() { return isRunning; }
    
    static SDL_Renderer *renderer;
    
private:
    int cnt = 0;
    bool isRunning = false;
    SDL_Window *window;
};

#endif /* Game_hpp */
