

#include "GameObject.hpp"
#include "TextureManager.hpp"


GameObject::GameObject(const char* textureSheet, int x, int y) {
    texture = TextureManager::LoadTexture(textureSheet);
    xpos = x;
    ypos = y;
}

GameObject::~GameObject() {
    
}

void GameObject::Update() {
    xpos++;
    ypos++;
    
    src.h = 32;
    src.w = 32;
    src.x = 0;
    src.y = 0;
    
    dest.x = xpos;
    dest.y = ypos;
    dest.h = src.h * 2;
    dest.w = src.w * 2;
}

void GameObject::Render() {
    SDL_RenderCopy(Game::renderer, texture, &src, &dest);
}
