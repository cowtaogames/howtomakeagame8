//
//  TextureManager.cpp
//  2D_Game_Engine
//
//  Created by Damon Floyd on 12/2/18.
//  Copyright © 2018 Damon Floyd. All rights reserved.
//

#include "TextureManager.hpp"

SDL_Texture* TextureManager::LoadTexture(const char *fileName) {
    SDL_Surface *tmpSurface = IMG_Load(fileName);
    if(!tmpSurface) {
        std::cout << "IMG_Load failed: " << IMG_GetError() << std::endl;
    }
    SDL_Texture *tex = SDL_CreateTextureFromSurface(Game::renderer, tmpSurface);
    SDL_FreeSurface(tmpSurface);
    
    return tex;
}

void TextureManager::Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest) {
    SDL_RenderCopy(Game::renderer, tex, &src, &dest);
}
