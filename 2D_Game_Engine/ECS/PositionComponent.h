//
//  PositionComponent.h
//  2D_Game_Engine
//
//  Created by Damon Floyd on 12/3/18.
//  Copyright © 2018 Damon Floyd. All rights reserved.
//

#ifndef PositionComponent_h
#define PositionComponent_h

#include "Components.hpp"

class PositionComponent : public Component
{
private:
    int xpos;
    int ypos;
    
public:
    
    PositionComponent() {
        xpos = 0;
        ypos = 0;
    }
    
    PositionComponent(int x, int y) {
        xpos = x;
        ypos = y;
    }
    
    int x() { return xpos; }
    int y() { return ypos; }
    
    void init() override
    {
        xpos = 0;
        ypos = 0;
    }
    
    void update() override
    {
        xpos++;
        ypos++;
    }
    
    void setPosition(int x, int y)
    {
        xpos = x;
        ypos = y;
    }
};

#endif /* PositionComponent_h */
