//
//  SpriteComponent.h
//  2D_Game_Engine
//
//  Created by Damon Floyd on 12/3/18.
//  Copyright © 2018 Damon Floyd. All rights reserved.
//

#ifndef SpriteComponent_h
#define SpriteComponent_h

#include "Components.hpp"
#include "SDL2/SDL.h"

class SpriteComponent : public Component {
private:
    PositionComponent *position;
    SDL_Texture *texture;
    SDL_Rect src, dest;
    
public:
    SpriteComponent() = default;
    SpriteComponent(const char* path)
    {
        setTex(path);
    }
    
    void setTex(const char* path) {
        texture = TextureManager::LoadTexture(path);
    }
    
    void init() override
    {
        std::cout << "SpriteComponent init" << std::endl;
        position = &entity->getComponent<PositionComponent>();
        
        src.x = src.y = 0;
        src.w = src.h = 32;
        
        dest.w = dest.h = 64;
    }
    
    void update() override
    {
        //std::cout << "SpriteComponent update: (" << dest.x << ", " << dest.y << ")" << std::endl;
        dest.x = position->x();
        dest.y = position->y(); 
    }
    
    void draw() override
    {
        // std::cout << "SpriteComponent draw called" << std::endl;
        TextureManager::Draw(texture, src, dest);
    }
};

#endif /* SpriteComponent_h */
